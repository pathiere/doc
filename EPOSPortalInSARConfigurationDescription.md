# TCS SATD InSAR web service configuration description

## Introduction
In this document, a description of the available parameters for setting up the query to the InSAR product web services through the EPOS ICS portal is provided.

The following instructions apply to all the following InSAR services (further described [here](https://gitlab.com/epos-tcs-satdata/doc/-/blob/main/TCS_SATD_Product_Description.md)): [Wrapped Interferograms](https://gitlab.com/epos-tcs-satdata/doc/-/blob/main/TCS_SATD_Product_Description.md#wrapped-interferograms-inw); [Unwrapped Interferograms](https://gitlab.com/epos-tcs-satdata/doc/-/blob/main/TCS_SATD_Product_Description.md#unwrapped-interferograms-inu); [Spatial Coherence](https://gitlab.com/epos-tcs-satdata/doc/-/blob/main/TCS_SATD_Product_Description.md#spatial-coherence-coh); [Map of LOS Vector](https://gitlab.com/epos-tcs-satdata/doc/-/blob/main/TCS_SATD_Product_Description.md#map-of-los-vector-cosneu); [Lookup table from radar coordinates to ground coordinates](https://gitlab.com/epos-tcs-satdata/doc/-/blob/main/TCS_SATD_Product_Description.md#lookup-table-from-radar-coordinates-to-ground-coordinates-lut); [LOS Displacement Time Series](https://gitlab.com/epos-tcs-satdata/doc/-/blob/main/TCS_SATD_Product_Description.md#los-displacement-time-series-dtslos); [Interferogram Atmospheric Phase Screen from Global Atmospheric Model](https://gitlab.com/epos-tcs-satdata/doc/-/blob/main/TCS_SATD_Product_Description.md#interferogram-atmospheric-phase-screen-from-global-atmospheric-model-aps); [DEM in radar geometry](https://gitlab.com/epos-tcs-satdata/doc/-/blob/main/TCS_SATD_Product_Description.md#dem-in-radar-geometry-dem)

Further web service/API parameters are available [here](https://docs.terradue.com/ellip/core/da/catalog/search.html#)

## Advanced search filters
The *Advanced search filters* allow the user to override the default query parameter to refine the search of SATD products. 

In the following a description of every search field/parameter is provided.

* ### *Spatial and Temporal Query*

    A **specific spatial** and **temporal search** can be done by activating the override switch, respectively. All the products intersecting the selected spatial box and  temporal span will be returned. Note that, in this case, the spatial search can not be done by using the graphical tools, but the ***North***, ***South***, ***West*** and ***East*** coordinates (in decimal degree) of the custom search box have to be inserted in the available fields, respectively.


|<p align = "center"><img src="portalPictures/SpatialTemporalSearch.png" alt="Override Spatial and Temporal search" width="70%"/></p>|
|:--:|

* ### *Data provider*

    It permits to list the products provided by only **one** of the TCS **Data Provider**. If left EMPTY, the products of all the relevant Data Providers are returned.

|<p align = "center"><img src="portalPictures/DataProv.png" alt="Data Provider refinement" width="70%"/></p>|
|:--:|

* ### *Number of returned results*

    It permits to set the **maximum number of returned products** per each query. Default value is ***20***.

---

* ### *Orbit Direction*

    It permits to discriminate the products according to the sensor orbiting direction. Possible values are: 
    * ***Ascending*** (products generated with data that are acquired by a sensor orbiting from South to North);
    * ***Descending*** (products generated with data that are acquired by a sensor orbiting from North to South). 

    If left EMPTY all the product will be shown.

|<p align = "center"><img src="portalPictures/OrbitDirection.png" alt="Orbit Direction" width="70%"/></p>|
|:--:|

* ### *Product Name (insert the Product Name to find a specific product)*

    If a specific product name is inserted in this field, only the corresponding product is returned by the query. This can be particularly **helpful when several products are overlapped** and the user would like to select just one of those listed in the table. The user can find the product name (to be pasted in this field) in the pop-up window or in the table list. Note that **no wildcards** are allowed.  
    **_WARNING_**: Inserting a wrong name will return an empty list.

---

* ### *Relative Orbit Number (Track)*

    It permits to list the product generated with data acquired along a **specific flight path** (**_Track_**), if known. The Track is the count of orbits from 1 to the number of orbits contained in a satellite repeat cycle. Track information is available in the pop-up/table of each product. Allowed values are positive integers.

---

* ### *Satellite platform*

    This parameter permits the user to specify the **satellite** used to generate the products. Possible values: ***S1*** (Sentinel-1).

---

* ### *Search Area in WKT format for custom polygons: POLYGON((Lon1 Lat1, Lon2 Lat2, ..., Lon1 Lat1))*

    This field permits to perform a spatial search with a custom polygon (provided in the **Well-known Text** format). The string format is the following: `POLYGON((Lon1 Lat1, Lon2 Lat2, ..., Lon1 Lat1))`  
    Each couple of values represents a vertex of the polygon. The last point shall be equal to the first one.
