# EPOS Satellite Data (SATD) Thematic Core Service Documentation

This project contains the Product and Service documentation of the EPOS SATD TCS.

The following documents are available:

* [Product Description](https://gitlab.com/epos-tcs-satdata/doc/-/blob/main/TCS_SATD_Product_Description.md)
* [EPOS Portal InSAR Parameter Configuration](https://gitlab.com/epos-tcs-satdata/doc/-/blob/main/EPOSPortalInSARConfigurationDescription.md)
